@extends('layout.master')

@section('judul')
    TAMBAH DATA CAST
@endsection

@section('content')

<form action="/cast" method="post">
    @csrf
    <div class="mb-3">
      <label for="nama" class="form-label">Nama</label>
      <input type="text" class="form-control" id="nama" name="nama">
      
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="mb-3">
      <label for="umur" class="form-label">Umur</label>
      <input type="number" class="form-control" id="umur" name="umur">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="mb-3">
        <label for="bio" class="form-label">Bio</label> <br>
        <textarea name="bio" id="bio" cols="163" rows="10"></textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection