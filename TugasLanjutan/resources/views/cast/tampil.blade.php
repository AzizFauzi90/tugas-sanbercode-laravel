@extends('layout.master')

@section('judul')
    TAMPIL DATA CAST
@endsection

@section('content')

<a href="cast/create" class="btn btn-primary btn-sm">Tambah</a>

<table class="table mt-4">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $value)
            <tr>
                <td> {{$key + 1}} </td>
                <td> {{$value->nama}} </td>
                <td> 
                    
                    <form action="/cast/{{$value->id}}" method="post">
                        @csrf
                        @method('DELETE')

                        <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Detail</a> 
                        <a href="/cast/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        
                        <input type="submit" value="Hapus" class="btn btn-danger btn-sm">
                    </form>
                </td>

            </tr>
        @empty
            <tr>
                <td>Tidak Ada Data</td>
            </tr>
        @endforelse
    </tbody>
  </table>
@endsection