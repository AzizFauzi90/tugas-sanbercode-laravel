@extends('layout.master')

@section('judul')
    EDIT DATA CAST
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="post">
    @csrf
    @method('PUT')
    
    <div class="mb-3">
      <label for="nama" class="form-label">Nama</label>
      <input type="text" class="form-control" id="nama" name="nama" value="{{$cast->nama}}">
      
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="mb-3">
      <label for="umur" class="form-label">Umur</label>
      <input type="number" class="form-control" id="umur" name="umur" value="{{$cast->umur}}">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="mb-3">
        <label for="bio" class="form-label">Bio</label> <br>
        <textarea name="bio" id="bio" cols="163" rows="10">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection