@extends('layout.master')

@section('judul')
    DETAIL DATA CAST
@endsection

@section('content')

<h1>{{$cast->nama}}</h1>
<b>Umur:</b>
<p>{{$cast->umur}}</p>
<b>Biodata:</b>
<p> {{$cast->bio}} </p>

<a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>
@endsection