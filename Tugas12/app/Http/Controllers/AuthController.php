<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function welcome(Request $req){
        $namaDepan = $req['firstname'];
        $namaBelakang = $req['lastname'];
        return view('welcome', ['namaDepan'=>$namaDepan, 'namaBelakang'=>$namaBelakang]);
    }
}
