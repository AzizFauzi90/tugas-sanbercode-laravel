<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
    @csrf
        <label for="firstname">First name:</label> <br> 
        <input id="firstname" type="text" name="firstname"> <br> <br>
        <label for="lastname">Last name:</label> <br> 
        <input id="lastname" type="text" name="lastname"> <br> <br>

        <label>Gender:</label> <br>
        <input type="radio" name="gender" value="male"> Male <br>
        <input type="radio" name="gender" value="female"> Female <br> 
        <input type="radio" name="gender" value="other"> Other <br><br>

        <label>Nasionality:</label> <br> 
        <select name="nasionality">
            <option value="indonesia">Indonesia</option>
            <option value="malaysia">Malaysia</option>
            <option value="singapore">Singapore</option>
            <option value="australia">Australia</option>
        </select>
        <br><br>

        <label>Language Spoken:</label> <br>
        <input type="checkbox"> Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br> <br>

        <label for="bio">Bio: </label> <br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
        <br><br>

        <input type="submit" value="Sign Up">
        <br><br>
    </form>
    

</body>
</html>