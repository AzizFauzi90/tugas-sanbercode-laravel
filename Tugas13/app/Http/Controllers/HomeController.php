<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(){
        return view('welcome');
    }

    public function table(){
        return view('halaman.table');
    }

    public function data_tables(){
        return view('halaman.data_tables');
    }

    public function master(){
        return view('layout.master');
    }
}
