<?php
    // release 0
    require("animal.php");
    require('Frog.php');
    require('Ape.php');

    $sheep = new Animal("shaun");

    echo "Name : $sheep->name <br>"; // "shaun"   
    echo "Legs : $sheep->legs <br>"; // 4
    echo "Cold_blooded : $sheep->cold_blooded <br><br>"; // "no"


    // release 1  
    $kodok = new Frog("buduk");
    echo "Name : $kodok->name <br>";    
    echo "Legs : $kodok->legs <br>"; 
    echo "Cold_blooded : $kodok->cold_blooded <br>"; 
    echo "Jump : "; 
    $kodok->jump(); // "hop hop"
    echo "<br>" ; 
    

    echo "<br>";

    $sungokong = new Ape("kera sakti");
    echo "Name : $sungokong->name <br>";    
    echo "Legs : $sungokong->legs <br>"; 
    echo "Cold_blooded : $sungokong->cold_blooded <br>"; 
    echo "Yell : "; 
    $sungokong->yell(); // "Auooo"
    echo "<br>"; 

?>