<?php  
    function ubah_huruf($string){
        $huruf = "abcdefghijklmnopqrstuvwxyza";
        $hasil = $string;
        $panjang = strlen($string);
        for($i = 0 ; $i < $panjang; $i++){
            $search = strpos($huruf, $string[$i]);
            $hasil[$i] = $huruf[$search+1];
        }

        return $hasil;
    }

    // TEST CASES
    echo ubah_huruf('wow'); // xpx
    echo '<br>';
    echo ubah_huruf('developer'); // efwfmpqfs
    echo '<br>';
    echo ubah_huruf('laravel'); // mbsbwfm
    echo '<br>';
    echo ubah_huruf('keren'); // lfsfo
    echo '<br>';
    echo ubah_huruf('semangat'); // tfnbohbu
    echo '<br>';
?>